import os
import streamlit as st
from streamlit_chat import message

from chat_utils import load_chain, get_text
import constants as constants

st.set_page_config(
    page_title="Streamlit Chat - Demo",
    page_icon=":robot:"
)

st.header("LangChat - Demo")

if 'chain' not in st.session_state:
    chain = load_chain()
    st.session_state["chain"] = chain

if 'generated' not in st.session_state:
    st.session_state['generated'] = []

if 'past' not in st.session_state:
    st.session_state['past'] = []

user_input = get_text()

if user_input:

    output = st.session_state.chain.run(human_input=user_input)
    
    st.session_state.past.append(user_input)
    st.session_state.generated.append(output)

if st.session_state['generated']:

    for i in range(len(st.session_state['generated'])-1, -1, -1):
        message(st.session_state["generated"][i], key=str(i))
        message(st.session_state['past'][i], is_user=True, key=str(i) + '_user')