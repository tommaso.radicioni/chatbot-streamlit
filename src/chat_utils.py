import os

from langchain import HuggingFaceHub, ConversationChain, LLMChain, PromptTemplate
from langchain.chains.conversation.memory import ConversationBufferMemory, ConversationSummaryBufferMemory

import constants as constants
import streamlit as st

def get_text():
    input_text = st.text_input("You: ","", key="input")
    return input_text 

def load_chain():
    """Logic for loading the chain you want to use should go here."""
    llm = HuggingFaceHub(
            repo_id="google/flan-t5-xl", 
            model_kwargs={"temperature":1e-10}
            )

    prompt = PromptTemplate(
        input_variables=["history", "human_input"], 
        template=constants.TEMPLATE
    )

    memory = ConversationSummaryBufferMemory(
        llm=llm,
        max_token_limit=constants.MAX_TOKEN_LIMIT)

    chain = LLMChain(
            llm=llm, 
            prompt=prompt,
            verbose=True,
            memory=memory
            )

    return chain