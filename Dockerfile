FROM python:3.8

# for streamlit
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

WORKDIR /code
ADD requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY src ./src

CMD streamlit run src/chatbot.py --server.port 8000
