# Chatbot example with Streamlit and Langchain

Steamlit is a library to create interactive web demo while Langchain is a library for creating chatbots, questions answering systems and other NLP tools.

## Run with docker

```
docker-compose up
# go to localhost:8000
```

## Local dev

* prepare environment
```
python3.8 -m venv venv

source venv/bin/activate # remember to activate the env eny times if you use it
# or for fish shell: source venv/bin/activate.fish

pip install -r requirements.txt
```

* run server
```
streamlit run src/chatbot.py
```

# References
* python
* venv
* docker
* docker-compose
* [streamlit](https://streamlit.io/)
* [langchain](https://github.com/hwchase17/langchain)
